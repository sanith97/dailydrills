const data = require('./question1.js');

let countries = [];

data.forEach(obj => {
    let salaryString = obj['salary'];
    let modifiedString = "";

    for(let str of salaryString) {
        if((typeof Number(str) === "number") && (str !== "$")) {
            modifiedString += str
        } 

    }
    
    obj['salary'] = Number(modifiedString);
    countries.push(obj['location'])
})

let totalCountries = countries.length;
let requireArray = [];

countries.forEach(country => {
    let returnedAverage = getAverageSalary(country);
    let requireObj = {};
    requireObj[country] = returnedAverage;

    requireArray.push(requireObj);
})

function getAverageSalary(country) {

    let salaryArray = [];

    data.forEach(obj => {
        
        if(obj['location'] == country) {

            salaryArray.push(obj['salary']);

        }
    })

    let coutriesSalarySum = salaryArray.reduce((preVal, currVal) => preVal + currVal);

    return (coutriesSalarySum / totalCountries);
}

console.log(requireArray);
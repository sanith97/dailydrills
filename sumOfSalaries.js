const data = require('./question1.js');

const salariesObj = data.map((obj) => {
    obj['salary'] = Number(obj['salary'].replace("$", ""))
    return obj;
})

const sumOfSalaries = salariesObj.map((obj) => obj['salary']).reduce((previousValue, currentValue) => {
    return previousValue + currentValue;
})

console.log(`Sum of all salaries in data is ${sumOfSalaries}`);

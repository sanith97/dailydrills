const data = require('./question1.js');

let salariesArray = [];

data.forEach(obj => {
    let salaryString = obj['salary'];
    let modifiedString = "";

    for(let str of salaryString) {
        if((typeof Number(str) === "number") && (str !== "$")) {
            modifiedString += str
        } 

    }
    
    salariesArray.push(Number(modifiedString));
    
})

let sumOfSalaries = salariesArray.reduce((preVal, currVal) => preVal + currVal);

console.log(`Sum of all salaries in data is ${sumOfSalaries}`);

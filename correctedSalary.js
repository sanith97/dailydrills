const data = require('./question1.js');

const correctedSalary = data.map((obj) => {
    obj['corrected_salary'] = Number(obj['salary'].replace("$", "")) * 10000;
    return obj;
});

console.log(correctedSalary);

